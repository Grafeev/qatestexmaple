import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class InitWebDriver {

    public static WebDriver driver;

    @BeforeClass
    public static void SetupWebDriver()
    {
        System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
        driver= new ChromeDriver();
        System.out.println("start");
    }

    @AfterClass
    public static void CloseWebDriver()
    {
        System.out.println("end");
        driver.quit();
    }

}
