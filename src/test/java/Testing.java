import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Testing extends InitWebDriver{

    PageElements elements;

    private boolean IsElementFound(WebElement element)
    {
        if(element != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private String GenerateRandomString()
    {
        int length = 10;
        String dictionary = "QqWwEeRrTtYyUuIiOoPpAaSsDdFfGgHhJjKkLlZzXxCcVvBbNnMm0123456789";
        String generateString = "";

        for(int i = 0; i < length; i++)
        {
            generateString += dictionary.charAt((int)(Math.random() * dictionary.length()));
        }

        return generateString;
    }

    @Test
    public  void test_1()
    {
        //System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
        //WebDriver driver = new ChromeDriver();
        elements = new PageElements(driver);
        driver.get("http://igorakintev.ru/admin/");

        elements.GetLogin_Input().sendKeys("Selenium");
        elements.GetPassword_Input().sendKeys("super_password");
        elements.GetSignIn_Button().click();

        assert (IsElementFound(elements.GetControlPanel_Header()));

        //===================================

        elements.GetAddEntry_Href().click();
        assert (IsElementFound(elements.GetAddEntry_Header()));

        String titleEntry = GenerateRandomString();
        elements.GetEntryTitle_input().sendKeys(titleEntry);
        elements.GetEntrySlug_input().sendKeys(GenerateRandomString());
        elements.GetEntryTextMarkdown_Textarea().sendKeys(GenerateRandomString());
        elements.GetEntryText_Textarea().sendKeys(GenerateRandomString());
        elements.GetSave_Button().click();
        driver.get("http://igorakintev.ru/blog/");

        assert (IsElementFound(elements.GetEntryTitleInBlog(titleEntry)));

        driver.get("https://igorakintev.ru/admin/blog/entry/");
        elements.GetEntryInTable(titleEntry).click();
        elements.GetDeleteEntryInPanel_Href().click();
        elements.GetDeleteEntry_Button().click();

        assert (!IsElementFound(elements.GetEntryInTable(titleEntry)));
    }

}
