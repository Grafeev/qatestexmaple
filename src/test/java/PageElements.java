import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PageElements {

    private WebDriver _driver;

    public By login_input = By.id("id_username");
    public By password_input = By.id("id_password");
    public By signIn_button = By.cssSelector("input[type='submit'][value='Войти']"); //<input type="submit" value="Войти">
    public By controlPanel_header = By.xpath("//h1[text()='Панель управления']");
    public By addEntry_href = By.cssSelector("a[href='/admin/blog/entry/add/']");
    public By addEntry_header = By.xpath("//h1[text()='Добавить entry']");
    public By entryTitle_input = By.id("id_title");
    public By entrySlug_input = By.id("id_slug");
    public By entryTextMarkdown_textarea = By.id("id_text_markdown");
    public By entryText_textarea = By.id("id_text");
    public By saveEntry_button = By.cssSelector("input[type='submit'][value='Сохранить'][name='_save']");
    public By deleteEntryInPanel_href = By.xpath("//p[@class='deletelink-box']/a[@class='deletelink']");
    public By deleteEntry_button = By.cssSelector("input[type='submit'][value='Да, я уверен']");


    PageElements(WebDriver driver)
    {
        _driver = driver;
    }

    public WebElement GetLogin_Input()
    {
        try
        {
            return _driver.findElement(login_input);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetPassword_Input()
    {
        try
        {
            return _driver.findElement(password_input);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetSignIn_Button()
    {
        try
        {
            return _driver.findElement(signIn_button);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetControlPanel_Header()
    {
        try
        {
            return _driver.findElement(controlPanel_header);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetAddEntry_Href()
    {
        try
        {
            return _driver.findElement(addEntry_href);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetAddEntry_Header()
    {
        try
        {
            return _driver.findElement(addEntry_header);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetEntryTitle_input()
    {
        try
        {
            return _driver.findElement(entryTitle_input);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetEntrySlug_input()
    {
        try
        {
            return _driver.findElement(entrySlug_input);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetEntryTextMarkdown_Textarea()
    {
        try
        {
            return _driver.findElement(entryTextMarkdown_textarea);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetEntryText_Textarea()
    {
        try
        {
            return _driver.findElement(entryText_textarea);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetSave_Button()
    {
        try
        {
            return _driver.findElement(saveEntry_button);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetEntryTitleInBlog(String title)
    {
        try
        {
            return _driver.findElement(By.xpath(String.format("//div[@id='entries']/h2/a[text()='%s']", title)));
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetEntryInTable(String title)
    {
        try
        {
            return _driver.findElement(By.xpath(String.format("//a[text()='%s']", title)));
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetDeleteEntryInPanel_Href()
    {
        try
        {
            return _driver.findElement(deleteEntryInPanel_href);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }

    public WebElement GetDeleteEntry_Button()
    {
        try
        {
            return _driver.findElement(deleteEntry_button);
        }
        catch (NoSuchElementException ex)
        {
            return null;
        }
    }
}
